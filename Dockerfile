FROM openjdk:17

ADD target/reactive-mongo-java.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]