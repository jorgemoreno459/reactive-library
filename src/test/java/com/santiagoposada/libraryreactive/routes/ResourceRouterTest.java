package com.santiagoposada.libraryreactive.routes;

import com.santiagoposada.libraryreactive.dto.ResourceDTO;
import com.santiagoposada.libraryreactive.usecase.CreateResourceUseCase;
import com.santiagoposada.libraryreactive.usecase.GetAllUseCase;
import com.santiagoposada.libraryreactive.usecase.GetResourceByIdUseCase;
import com.santiagoposada.libraryreactive.usecase.UpdateUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@WebFluxTest
class ResourceRouterTest {

    private WebTestClient webTestClient;
    private CreateResourceUseCase createResourceUseCase;
    private GetAllUseCase getAllUseCase;
    private GetResourceByIdUseCase getResourceByIdUseCase;
    private UpdateUseCase updateUseCase;
    @BeforeEach
    void setup() {
        createResourceUseCase = Mockito.mock(CreateResourceUseCase.class);
        getAllUseCase = Mockito.mock(GetAllUseCase.class);
        getResourceByIdUseCase = Mockito.mock(GetResourceByIdUseCase.class);
        updateUseCase = Mockito.mock(UpdateUseCase.class);
    }
    @Test
    public void createResourceRoute() {

        ResourceDTO resourceDTO = new ResourceDTO();
        resourceDTO.setId("1233435ff");
        resourceDTO.setName("test 1");
        resourceDTO.setType("Tipo #1");
        resourceDTO.setCategory("Area tematica #1");
        resourceDTO.setUnitsAvailable(2);
        resourceDTO.setUnitsOwed(0);
        resourceDTO.setLastBorrow(LocalDate.parse("2020-01-10"));

        when(createResourceUseCase.apply(resourceDTO)).thenReturn(Mono.just(resourceDTO));

        webTestClient = WebTestClient
                .bindToRouterFunction(new ResourceRouter().createResourceRoute(createResourceUseCase))
                .build();

        webTestClient.post().uri("/create")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(resourceDTO), ResourceDTO.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ResourceDTO.class)
                .value(response -> {
                    assertEquals("test 1", response.getName());
                });
    }

    @Test
    void getAllRouter() {
        List<ResourceDTO> resourcesDTO = Arrays.asList(
                new ResourceDTO(),
                new ResourceDTO()
        );

        when(getAllUseCase.get()).thenReturn(Flux.fromIterable(resourcesDTO));

        webTestClient = WebTestClient
                .bindToRouterFunction(new ResourceRouter().getAllRouter(getAllUseCase))
                .build();

        webTestClient.get().uri("/resources")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ResourceDTO.class)
                .hasSize(resourcesDTO.size())
                .contains(resourcesDTO.toArray(new ResourceDTO[0]));
    }

    @Test
    void getResourceById() {
        ResourceDTO resourceDTO = new ResourceDTO();
        resourceDTO.setId("1233435ff");

        when(getResourceByIdUseCase.apply(resourceDTO.getId())).thenReturn(Mono.just(resourceDTO));

        webTestClient = WebTestClient
                .bindToRouterFunction(new ResourceRouter().getResourceById(getResourceByIdUseCase))
                .build();

        webTestClient.get().uri("/resource/{id}",resourceDTO.getId())
                .exchange()
                .expectStatus().isOk()
                .expectBody(ResourceDTO.class)
                .isEqualTo(resourceDTO);
    }

    @Test
    void updateResourceRoute() {
        ResourceDTO resourceDTO = new ResourceDTO();
        resourceDTO.setId("1233435ff");
        resourceDTO.setName("test 2");
        resourceDTO.setType("Tipo #1");
        resourceDTO.setCategory("Area tematica #1");
        resourceDTO.setUnitsAvailable(2);
        resourceDTO.setUnitsOwed(0);
        resourceDTO.setLastBorrow(LocalDate.parse("2020-01-10"));

        when(updateUseCase.apply(resourceDTO)).thenReturn(Mono.just(resourceDTO));

        webTestClient = WebTestClient
                .bindToRouterFunction(new ResourceRouter().updateResourceRoute(updateUseCase))
                .build();

        webTestClient.put().uri("/update")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(resourceDTO), ResourceDTO.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ResourceDTO.class)
                .value(response -> {
                    assertEquals("test 2", response.getName());
                });
    }

    @Test
    void deleteResourceToute() {
    }

    @Test
    void checkForAvailabilityRoute() {
    }

    @Test
    void getByTypeRoute() {
    }

    @Test
    void getByCategory() {
    }

    @Test
    void borrowResourceRoute() {
    }

    @Test
    void returnRoute() {
    }
}