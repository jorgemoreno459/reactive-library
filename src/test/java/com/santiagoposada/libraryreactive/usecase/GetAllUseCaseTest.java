package com.santiagoposada.libraryreactive.usecase;

import com.santiagoposada.libraryreactive.dto.ResourceDTO;
import com.santiagoposada.libraryreactive.entity.Resource;
import com.santiagoposada.libraryreactive.mapper.ResourceMapper;
import com.santiagoposada.libraryreactive.repository.ResourceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class GetAllUseCaseTest {
    private ResourceRepository resourceRepository;
    private ResourceMapper resourceMapper;
    private GetAllUseCase getAllUseCase;
    @BeforeEach
    void setup(){
        resourceMapper = Mockito.mock(ResourceMapper.class);
        resourceRepository = Mockito.mock(ResourceRepository.class);
        getAllUseCase = new GetAllUseCase(resourceMapper,resourceRepository);
    }
    @Test
    void get() {
        List<Resource> resources = Arrays.asList(
            new Resource("1", "test 1", "test","test",LocalDate.parse("2020-01-10"),2,0),
            new Resource("2", "test 2", "test2","test2",LocalDate.parse("2020-01-10"),2,0)
        );

        List<ResourceDTO> resourcesDTO = Arrays.asList(
                new ResourceDTO(),
                new ResourceDTO()
        );

        when(resourceMapper.fromResourceEntityToDTO()).thenReturn(resource -> {
            return new ResourceDTO();
        });
        when(resourceRepository.findAll()).thenReturn(Flux.fromIterable(resources));

        Flux<ResourceDTO> resultFlux = getAllUseCase.get();
        StepVerifier.create(resultFlux)
                .expectNext(resourcesDTO.get(0))
                .expectNext(resourcesDTO.get(1))
                .verifyComplete();
    }
}