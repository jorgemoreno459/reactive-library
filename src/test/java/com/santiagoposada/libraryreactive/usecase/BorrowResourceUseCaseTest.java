package com.santiagoposada.libraryreactive.usecase;

import com.santiagoposada.libraryreactive.dto.ResourceDTO;
import com.santiagoposada.libraryreactive.entity.Resource;
import com.santiagoposada.libraryreactive.mapper.ResourceMapper;
import com.santiagoposada.libraryreactive.repository.ResourceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import org.mockito.Mockito;

class BorrowResourceUseCaseTest {
    private ResourceRepository resourceRepository;
    private UpdateUseCase updateUseCase;
    private ResourceMapper resourceMapper;
    private BorrowResourceUseCase borrowResourceUseCase;
    @BeforeEach
    void setup(){
        resourceMapper = Mockito.mock(ResourceMapper.class);
        resourceRepository = Mockito.mock(ResourceRepository.class);
        updateUseCase = Mockito.mock(UpdateUseCase.class);
        borrowResourceUseCase = new BorrowResourceUseCase(resourceMapper,resourceRepository,updateUseCase);
    }
    @Test
    @DisplayName("just apply")
    void apply() {
        Resource resource = new Resource();
        resource.setId("1233435ff");
        resource.setName("Nombre #1");
        resource.setType("Tipo #1");
        resource.setCategory("Area tematica #1");
        resource.setUnitsAvailable(2);
        resource.setUnitsOwed(0);
        resource.setLastBorrow(LocalDate.parse("2020-01-10"));

        ResourceDTO resourceDTO = new ResourceDTO();
        resourceDTO.setId(resource.getId());
        resourceDTO.setName(resource.getName());
        resourceDTO.setType(resource.getType());
        resourceDTO.setCategory(resource.getCategory());
        resourceDTO.setUnitsAvailable(resource.getUnitsAvailable());
        resourceDTO.setUnitsOwed(resource.getUnitsOwed());
        resourceDTO.setLastBorrow(resource.getLastBorrow());

        when(resourceRepository.findById(resource.getId())).thenReturn(Mono.just(resource));
        when(resourceMapper.fromResourceEntityToDTO()).thenReturn(r -> resourceDTO);
        when(updateUseCase.apply(any(ResourceDTO.class))).thenReturn(Mono.empty());

        StepVerifier.create(borrowResourceUseCase.apply(resource.getId()))
                .expectNext("The resource Nombre #1 has been borrowed, there are 1 units available")
                .verifyComplete();

        verify(updateUseCase, times(1)).apply(any(ResourceDTO.class));
    }

    @Test
    void testApplyWhenNoUnitsAvailable() {
        String id = "1233435ff";
        Resource resource = new Resource();
        resource.setUnitsAvailable(0);
        when(resourceRepository.findById(id)).thenReturn(Mono.just(resource));

        StepVerifier.create(borrowResourceUseCase.apply(id))
                .expectNext("There arent units left to be borrow of that resource")
                .verifyComplete();

        verify(updateUseCase, never()).apply(any(ResourceDTO.class));
    }
}