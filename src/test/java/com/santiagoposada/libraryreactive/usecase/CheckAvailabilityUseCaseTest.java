package com.santiagoposada.libraryreactive.usecase;

import com.santiagoposada.libraryreactive.entity.Resource;
import com.santiagoposada.libraryreactive.mapper.ResourceMapper;
import com.santiagoposada.libraryreactive.repository.ResourceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class CheckAvailabilityUseCaseTest {

    private ResourceMapper resourceMapper;
    private ResourceRepository resourceRepository;
    private CheckAvailabilityUseCase checkAvailabilityUseCase;

    @BeforeEach
    void setup(){
        resourceMapper = Mockito.mock(ResourceMapper.class);
        resourceRepository = Mockito.mock(ResourceRepository.class);
        checkAvailabilityUseCase = new CheckAvailabilityUseCase(resourceMapper,resourceRepository);
    }

    @Test
    @DisplayName("units available")
    void apply() {
        String id = "1233435ff";
        Resource resource = new Resource();
        resource.setName("test");
        resource.setUnitsAvailable(1);
        when(resourceRepository.findById(id)).thenReturn(Mono.just(resource));

        StepVerifier.create(checkAvailabilityUseCase.apply(id))
                .expectNext("test is available")
                .verifyComplete();

    }

    @Test
    @DisplayName("units not available")
    void applyWhenNotUnitsAvailable() {
        String id = "1233435ff";
        Resource resource = new Resource();
        resource.setName("test");
        resource.setUnitsAvailable(0);
        resource.setLastBorrow(LocalDate.parse("2020-01-10"));
        when(resourceRepository.findById(id)).thenReturn(Mono.just(resource));

        StepVerifier.create(checkAvailabilityUseCase.apply(id))
                .expectNext(resource.getName()+" is not available, last borrow " + resource.getLastBorrow())
                .verifyComplete();

    }
}