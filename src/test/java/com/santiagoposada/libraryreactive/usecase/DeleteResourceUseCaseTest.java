package com.santiagoposada.libraryreactive.usecase;

import com.santiagoposada.libraryreactive.entity.Resource;
import com.santiagoposada.libraryreactive.mapper.ResourceMapper;
import com.santiagoposada.libraryreactive.repository.ResourceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Mockito.*;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DeleteResourceUseCaseTest {


    private ResourceRepository resourceRepository;
    private ResourceMapper resourceMapper;
    private DeleteResourceUseCase deleteResourceUseCase;
    @BeforeEach
    void setup(){
        resourceMapper = Mockito.mock(ResourceMapper.class);
        resourceRepository = Mockito.mock(ResourceRepository.class);
        deleteResourceUseCase = new DeleteResourceUseCase(resourceMapper,resourceRepository);
    }

    @Test
    void apply() {
        Resource resource = new Resource();
        resource.setId("1233435ff");

        when(resourceRepository.deleteById(resource.getId())).thenReturn(Mono.empty());

        deleteResourceUseCase.apply(resource.getId())
                .as(StepVerifier::create)
                .verifyComplete();

        verify(resourceRepository, times(1)).deleteById(resource.getId());
    }
}